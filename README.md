# Getting Started with The App

## Available Scripts

In the project directory, you can run:

### `npm run serve`

Runs the app in the development mode.\
Server connects to [http://localhost:4000](http://localhost:4000) to view it in your browser.

### `npm test`

Runs the test files

Check out the Documentation here
[http://localhost:4000/api-docs](http://localhost:4000/api-docs)
