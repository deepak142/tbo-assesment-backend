const supertest = require('supertest');
const app = require('../server');

test('POST /do-maths 200 OK', async () => {
    // Check with numbers in body
    let numbers = "1,2,3,4,5";

    const response = await supertest(app).post('/do-maths').send({
        numbers
    });

    // Get results manually
    numbers = numbers.split(",");
    const sum = numbers.reduce((acc, curr) => acc + parseInt(curr), 0)
    const avg = sum / numbers.length;
    const stdev = Math.sqrt(numbers.reduce((acc, curr) => acc + Math.pow(parseInt(curr) - avg, 2), 0) / numbers.length)

    // Testcases
    expect(response.statusCode).toBe(200);

    // Check the results
    expect(response.body).toEqual({
        sum,
        avg,
        stdev
    });
});


test('POST /do-maths 400 BAD REQUEST', async () => {
    // Check without numbers in body
    const response = await supertest(app).post('/do-maths').send();

    // Testcases
    expect(response.statusCode).toBe(400);
    expect(response.text).toBe("Numbers are required");
})