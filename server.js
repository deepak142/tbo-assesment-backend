const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors")
const app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const morgan = require('morgan');

app.use(morgan("dev"));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(cors({
    origin: "http://localhost:3000"
}))

app.use(bodyParser.json());


app.post("/do-maths", async (req, res) => {
    if (!req.body.numbers) {
        return res.status(400).json({ numbers: ["Numbers are required"] });
    }

    const numbers = req.body.numbers.split(",");
    const sum = numbers.reduce((acc, curr) => acc + parseInt(curr), 0);
    const avg = sum / numbers.length;
    const stdev = Math.sqrt(numbers.reduce((acc, curr) => acc + Math.pow(parseInt(curr) - avg, 2), 0) / numbers.length);
    res.json({
        sum,
        avg,
        stdev,
    });
})

module.exports = app;